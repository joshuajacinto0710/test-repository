#1.	Write a program to print the following string in a specific format (see the output).
print("Twinkle, twinkle, little star,")
print("\tHow I wonder what you are!") 
print("\t\tUp above the world so high,")   		
print("\t\tLike a diamond in the sky.") 
print("Twinkle, twinkle, little star,") 
print("\tHow I wonder what you are")



#2.	Write a Python program to get the Python version you are using.
import sys
print(sys.version)



#3.	Write a Python program to calculate the length of a string.
word = "Hello World"
print(len(word))



#4.	Write a program that asks the user to enter their name and their age. Print out a message.
Name = input("Enter your name: ")
Age = input("Enter your age: ")
print("Your name is " + Name + " and your age is " + Age)



#5.	Write a Python program to display the current date and time.
import datetime
x = datetime.datetime.now()
print(x)



#6.	Write a Python program which accepts a sequence of comma-separated numbers from user and generate a list and a tuple with those numbers. 
num = input("Enter numbers separated by comma : ")
x = num.split(",") #spliting string int list
y = tuple(x) #converting list into tuple
print("List : ",x)
print("Tuple : ",y)



#7.	Write a Python program to display the first and last colors from the following list. 
color_list = ["Red","Green","White" ,"Black"]
color_list = ["Red","Green","White" ,"Black"]
x = str(list((color_list[0],color_list[-1]))) #convert the list to string value
print(x.replace("\'", "\"")) #replacing the value of single quote to double quote



#8.	Write a Python program to concatenate all elements in a list into a string and return it.
List = ['1a','2b','3c','4d','5e','6','7','8','9','0']
x = "".join(List) #replacing the value of single quote to double quote
print(x)	



#9.	Write a Python program that accepts an integer (n) and computes the value of n+nn+nnn.
"""Sample value of n is 5 
Expected Result : 615"""
num = input("Enter number: ")
temp1 = str(num) #convert input to string vtype
temp2 = temp1 + temp1 
temp3 = temp1 + temp1 + temp1
x = int(temp1)  #convert string to int type
y = int(temp2)
z = int(temp3)
print(x+y+z)
