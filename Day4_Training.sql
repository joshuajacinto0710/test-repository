/*Creating database*/
create database inet; 

/*Creating Table named "patients" and creating unique index for firstname, lastname and middlename*/
CREATE TABLE patients( 
patientId int primary key auto_increment,
firstname varchar(100) not null,
lastname varchar(100) not null,
middlename varchar(100),
birthdate date not null,
gender ENUM ('Male','Female'),
civilstatus ENUM ('Single','Married','Divorced','Widowed'),
address varchar(100) not null,
emailaddress varchar(20),
contactnumber varchar(20), 
CONSTRAINT Unique_Index UNIQUE (firstname,lastname,middlename) 
);


/*Creating Table named "examinations" and creating foreign keys between patients and examinations table*/
CREATE TABLE examinations(
examId int primary key auto_increment,
patientId int not null,
exam_name varchar(100) not null,
results varchar(100) not null,
date_created DATETIME not null,
CONSTRAINT FK_PatientsExaminations FOREIGN KEY (patientId)
    REFERENCES patients(patientId) 
);

/*Adding a new record on the patient table*/
INSERT INTO patients (firstname, lastname, middlename, birthdate, gender, civilstatus, address, emailaddress, contactnumber)
VALUES 
('James','Lebron','King','1990-06-12','Male','Married','USA','lebron@gmail','09092323232'),
('Michael','Jordan','Goat','1980-09-22','Male','Married','CANADA','goatMJ@gmail','0910001000'),
('Kai','Sotto','Kaiju','2000-06-03','Male','Single','PHILIPPINES','kaiju@gmail','09221234890')	
;

/*Updates 1 record on the patient table*/
UPDATE patients
SET address='USA'
WHERE patientId=2;

/*Removes 1 record on the patient table*/
DELETE FROM patients WHERE patientId=3;

/*Displays all the records on the patient table and arrange it by age*/
SELECT * FROM patients
ORDER BY birthdate DESC;

/*INSERT INTO examinations(patientId, exam_name, results, date_created)
VALUES 
('1','Math','Good','2010-06-06'),
('2','English','Good','2000-01-01')
;*/

/*Displays columns from patients and examinations tables*/
SELECT *
FROM patients, examinations
WHERE patients.patientId = examinations.patientId;

/*Counts all the records on the patient table*/
SELECT COUNT(patientId)
FROM patients;









