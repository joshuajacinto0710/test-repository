#1.	Write a Python program to calculate the number of days between two dates.
#Sample dates : (2014, 7, 2), (2014, 7, 11)
#Expected output : 9 days 
from datetime import date as dt
firstdate = dt(2014, 7, 2)
lastdate = dt(2014, 7, 11)
Day = lastdate - firstdate
print(Day.days,"days")



#2.	Write a Python program to find whether a given number (accept from the user) is even or odd, print out result  if the user input is odd or even
Input = input("Enter number:")
Number = int(Input) #converting to int
if ((Number % 2) == 0): # if the remainder is divisible by two
    print(Number, "is a even number")
else:
    print(Number, "is a odd number")



#3.	Write a Python program that test whether a number is within 100 of 1000 or 2000.
#The function returns the absolute value of a number.
#Test Data:(1000, 900, 800, and 2200) print 4 number.
#Expected output :
#1000 = True                                                                                                          
#900 = True                                                                                                          
#800 = False                                                                                                         
#2200 = False
def myfunc(x):
    return ((abs(x - 1000) <= 100) or (abs(x - 2000) <= 100)) #true if 1000 or 2000 - the value is less than or equal to 100 and return to absolute value
print(myfunc(1000))
print(myfunc(900))
print(myfunc(800))
print(myfunc(2200))



#4.	Write a Python program to compute the future value of a specified principal amount, rate of interest, and a number of years. 
#Test Data : amt = 10000, int = 3.5, years = 7
#Expected Output : 12722.79
amt = 10000
int = 3.5
years = 7
interest = int/100
futureValue = amt*((1+(interest))**years)
print(round(futureValue,2)) #print value with whole number and 2 decimal only



#5.	Write a Python program to get a string from a given string where all occurrences of its first char have been changed to '$', except the first char itself. 
#Sample String : 'restart'
#Expected Result : 'resta$t'
word = input("Enter word: ")
replaceWord = word.replace(word[0], "$")  #replace the all character
replaceWord = replaceWord.replace("$", word[0],1)  #replace the first character only
print(replaceWord)



#6.	Create a python function that accepts two parameters. It will check if the values are the same type.
#If two values are string, concatenate the two strings and return its value.
#If two values are an integer, multiply them and return its value.
#If two values are lists, add the two lists and return it as a single list.
#If two values are dictionaries, add the two dictionaries and return it as a single dictionary.
#If two values are datetime objects, add the two objects and return its value.
import datetime
x = datetime.datetime.now()

def myfunc(Val1,Val2):
    if isinstance(Val1 and Val2 , int):
        return Val1 * Val2
    if isinstance(Val1 and Val2 , str):
        return Val1 + Val2
    if isinstance(Val1 and Val2 , list):
        return Val1 + Val2
    if isinstance(Val1 and Val2 , dict):
        return Val1 | Val2
    if isinstance(Val1 and Val2 , datetime.datetime):
        return Val1 , Val2

print(myfunc("Hello ","World"))
print(myfunc(22,23))
print(myfunc(["apple", "banana", "cherry"],["orange", "grapes", "mango"]))
print(myfunc(
    {'John': 15, 'Misa' : 12 },{'Bonnie': 18,'Matt' : 16 }
    ))
print(myfunc(x.strftime("%x "),x.strftime("%A")))



#7.	Write a Python program to create a Pet class with dog_name, dog_breed and dog_color instance attributes.
#Sample Output: I bought a black labrador and I named my dog Max
class Pet:
  def __init__(self, dog_name, dog_breed, dog_color):
    self.dog_name = dog_name
    self.dog_breed = dog_breed
    self.dog_color = dog_color

  def dog(self):
    print("I bought a " + self.dog_color +" "+ self.dog_breed + "and I named my dog " + self.dog_name)

P = Pet("Max", "labrador","black")
P.dog()



#8.	Create a python class that inherits from the previous class created from number 7, and overrides the method dog_name to return the  dog_name all in uppercase.
class Pet:
  def __init__(self, dog_name, dog_breed, dog_color):
    self.dog_name = dog_name
    self.dog_breed = dog_breed
    self.dog_color = dog_color

  def dog(self):
    print("I bought a " + self.dog_color +" "+ self.dog_breed + "and I named my dog " + self.dog_name)

class Animal(Pet):
    def __init__(self, dog_name, dog_breed, dog_color):
        Pet.__init__(self, dog_name, dog_breed, dog_color)
        self.dog_name = dog_name.upper()

P = Animal("Max", "labrador","black")
P.dog()



#9.	Create 2 python classes that demonstrate the use of python’s super function.
class Pet:
  def __init__(self, dog_name, dog_breed, dog_color):
    self.dog_name = dog_name
    self.dog_breed = dog_breed
    self.dog_color = dog_color

  def dog(self):
    print("I bought a " + self.dog_color +" "+ self.dog_breed + "and I named my dog " + self.dog_name)

class Animal(Pet):
    def __init__(self, dog_name, dog_breed, dog_color, dog_action):
        super().__init__(dog_name, dog_breed, dog_color)
        self.dog_action = dog_action #adding new attributes dog_action
    
    def dogbite(self):
        print("I bought a " + self.dog_color +" "+ self.dog_breed + "and I named my dog " + self.dog_name + " " +self.dog_action)

P = Animal("Max", "labrador","black","bite")
P.dogbite()


 
