from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Profile, Posting
from .forms import PostingForm, SignUpForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django import forms


def home(request):
    if request.user.is_authenticated: # User can post if authenticated is True
        form = PostingForm(request.POST or None)
        if request.method == "POST":
            if form.is_valid():
                posting = form.save(commit=False)
                posting.user = request.user
                posting.save()
                messages.success(request, ("Your post had been added"))
                return redirect('home')

        # The data shows in order by created_at (New post is in the top) 
        postings = Posting.objects.all().order_by("-created_at")
        return render(request, 'home.html',{"postings":postings, "form":form})
    
    else:
        # The data shows in order by created_at (New post is in the top)
        postings = Posting.objects.all().order_by("-created_at")
        return render(request, 'home.html',{"postings":postings})



def profile_list(request):
    if request.user.is_authenticated:
        profiles = Profile.objects.exclude(user=request.user)
        return render(request, 'profile_list.html',{"profiles":profiles})
    
    else:
        messages.success(request, ("You Must Be Logged In to View this Page"))
        return redirect('home')



def profile(request, pk):
    if request.user.is_authenticated:
        profile = Profile.objects.get(user_id=pk)
        # Shows the data in order by created_at (New post is in the top)
        postings = Posting.objects.filter(user_id=pk).order_by("-created_at")

        # Post Form Logic
        if request.method == "POST":
            # Get current user id
            current_user_profile =  request.user.profile
            # Get form data
            action = request.POST['follow']
            # Decide to follow or unfollow
            if action == "unfollow":
                current_user_profile.follows.remove(profile)
        
            elif action == "follow":
                current_user_profile.follows.add(profile)
            #Save the profile
            current_user_profile.save()


        return render(request, 'profile.html', {"profile":profile,"postings":postings})
    else:
        messages.success(request, ("You Must Be Logged In to View this Page"))
        return redirect('home')
    


def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, ("You Have been logged in"))
            return redirect('home')
        else:
            messages.success(request, ("PLease Try Again"))
            return redirect('login')
    else:    
        return render(request, 'login.html', {})
    

def logout_user(request):
    logout(request)
    messages.success(request, ("You Have been logged out"))
    return redirect('home')


def register_user(request):
	form = SignUpForm()
	if request.method == "POST":
		form = SignUpForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			email = form.cleaned_data['email']
			# Log in user
			user = authenticate(username=username, password=password)
			login(request,user)
			messages.success(request, ("You have successfully registered!"))
			return redirect('home')
	
	return render(request, "register.html", {'form':form})


