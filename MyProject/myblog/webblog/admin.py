from django.contrib import admin
from django.contrib.auth.models import Group, User
from .models import Profile, Posting

# Unregister Group
admin.site.unregister(Group)

# Mix profiles into user form
class ProfileInline(admin.StackedInline):
    model = Profile

# Extends User Model.
class UserAdmin(admin.ModelAdmin):
    model = User
    # just display the username fields on admin page
    fields = ["username","email","first_name", "last_name", "password"]
    inlines = [ProfileInline]

# Unregister intial User
admin.site.unregister(User)

# Register User and Profile
admin.site.register(User, UserAdmin)

# Register Posting
admin.site.register(Posting)


