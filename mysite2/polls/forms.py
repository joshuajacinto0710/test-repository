from django import forms
from .models import Author, Book, TITLE_CHOICES
from django.forms import ModelForm

#class NameForm(forms.Form):
#    your_name = forms.CharField(label='Your name', max_length=100)

#class ContactForm(forms.Form):
#    subject = forms.CharField(max_length=100)
#    message = forms.CharField(widget=forms.Textarea)
#    sender = forms.EmailField()
#    cc_myself = forms.BooleanField(required=False)


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = '__all__'

class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = '__all__'






